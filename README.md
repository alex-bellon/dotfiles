
```
			██████╗  ██████╗ ████████╗███████╗██╗██╗     ███████╗███████╗
     			██╔══██╗██╔═══██╗╚══██╔══╝██╔════╝██║██║     ██╔════╝██╔════╝
     			██║  ██║██║   ██║   ██║   █████╗  ██║██║     █████╗  ███████╗
     			██║  ██║██║   ██║   ██║   ██╔══╝  ██║██║     ██╔══╝  ╚════██║
     			██████╔╝╚██████╔╝   ██║   ██║     ██║███████╗███████╗███████║
    			╚═════╝  ╚═════╝    ╚═╝   ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝
```                                                          

The dotfiles I use to customize my Linux setup. Includes files for i3, urxvt, zsh, vim, tmux, rofi, Firefox

## Technical Information
```
     Distro: Linux Mint 18.3 Sylvia 
   Terminal: urxvt + tmux
      Shell: zsh + Oh-My-Zsh
         WM: i3-gaps
      Icons: Paper (snwh.org/paper)
  GTK Theme: Materia-Dark-Compact (github.com/nana-4/materia-theme)
    Browser: Firefox
     Editor: Vim + Atom
       Font: Hack
```

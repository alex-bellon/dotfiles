i3-msg 'workspace Home; append_layout ~/Dotfiles/i3/home.json'
firefox --new-window 'https://todoist.com/app?lang=en#agenda%2Foverdue%20%26%20!assigned%20to%3A%20other%2C%207%20days%20%26%20!assigned%20to%3A%20other'
firefox 'https://calendar.google.com/calendar/r/week' 'https://mail.google.com/mail/u/0/#inbox' 'https://mail.google.com/mail/u/1/#inbox'
